package com.company.automation;

/**
 * Created by _tasmitt_ on 8/10/2016.
 */
public class RandomFromRange {
    public static int randomFromRange(double min, double max){
        return (int)(Math.random()*(max-min)+min);
    }
}
