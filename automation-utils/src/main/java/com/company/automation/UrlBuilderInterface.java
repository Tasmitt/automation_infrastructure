package com.company.automation;

/**
 * Created by _tasmitt_ on 21/9/2016.
 */
public interface UrlBuilderInterface {
        UrlBuilderInterface withSecureProtocol();

        UrlBuilderInterface withProductionDomain(String domain);

        UrlBuilderInterface withPort(String port);

        UrlBuilderInterface withPath(String path);

        UrlBuilderInterface withParam(String param);

        UrlBuilderInterface withParam(String key, String value);

        String build();
}
