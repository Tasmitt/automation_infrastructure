package com.company.automation;

import java.security.SecureRandom;

/**
 * Created by _tasmitt_ on 9/10/2016.
 */
public class RandomString {
    static final String AB = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
    static final String A1 = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
    static final String num = "0123456789";
    static SecureRandom rnd = new SecureRandom();


    public static String randomAlphabeticString(int len){
        StringBuilder sb = new StringBuilder( len );
        for( int i = 0; i < len; i++ )
            sb.append( AB.charAt( rnd.nextInt(AB.length()) ) );
        return sb.toString();

    }

    public static String randomAlphanumericString(int len){
        StringBuilder sb = new StringBuilder( len );
        for( int i = 0; i < len; i++ )
            sb.append( A1.charAt( rnd.nextInt(A1.length()) ) );
        return sb.toString();

    }
    public static String randomNumericString(int len){
        StringBuilder sb = new StringBuilder( len );
        for( int i = 0; i < len; i++ )
            sb.append( num.charAt( rnd.nextInt(num.length()) ) );
        return sb.toString();

    }

}
