package com.company.automation;

/**
 * Created by _tasmitt_ on 21/9/2016.
 */
public class UrlBuilder implements UrlBuilderInterface {
    private String prot = "http://";
    private String dom = "no.domain";
    private String porT = "/";
    private String parameters = "";
    private String patH = "";





    @Override
    public UrlBuilderInterface withSecureProtocol() {
        prot = "https://";
        return this;
    }

    @Override
    public UrlBuilderInterface withProductionDomain(String domain) {
        dom = domain;
        return this;
    }

    @Override
    public UrlBuilderInterface withPort(String port) {
        porT = ":"+ port + porT;
        return this;
    }

    @Override
    public UrlBuilderInterface withPath(String path) {
        patH = path;
        return this;
    }

    @Override
    public UrlBuilderInterface withParam(String param) {
        if (parameters==""){
            parameters = "?" + param;
            return this;
        } else {
            parameters = parameters + "&" + param;
        return this;
        }
    }


    @Override
    public UrlBuilderInterface withParam(String key, String value) {

        if (parameters==""){
            parameters = "?" + key + "=" + value;
            return this;
        } else {
            parameters = parameters + "&" + key + "=" + value;
            return this;
        }
    }

    @Override
    public String build() {
        return  prot+dom+porT+patH+parameters;

    }

}