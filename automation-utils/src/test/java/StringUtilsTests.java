import com.company.automation.RandomString;
import org.junit.Test;

import static org.junit.Assert.*;

public class StringUtilsTests {

    private int randomStringLength = 6;

    @Test
    public void testRandomNumericString() {
        String randomString = RandomString.randomNumericString(randomStringLength);
        assertTrue(randomString.length() == randomStringLength);
        assertTrue(randomString.matches("[0-9]*"));
    }

    @Test
    public void testRandomAlphaString() {
        String randomString = RandomString.randomAlphabeticString(randomStringLength);
        assertTrue(randomString.length() == randomStringLength);
        assertTrue(randomString.matches("[a-zA-Z]*"));
    }

    @Test
    public void testRandomAlphaNumericString() {
        String randomString = RandomString.randomAlphanumericString( randomStringLength);
        assertTrue(randomString.length() == randomStringLength);
        assertTrue(randomString.matches("[0-9a-zA-Z]*"));
    }
}
