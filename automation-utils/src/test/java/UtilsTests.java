import com.company.automation.RandomFromRange;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class UtilsTests {

    private int min = 43;
    private int max = 105;
    private int randomNumber;

    @Before
    public void setup() {
        randomNumber = RandomFromRange.randomFromRange(min, max);
    }

    @Test
    public void testRandomNumberBiggerThanMin() {
        assertTrue("randomNumber is less than min", min <= randomNumber);
    }

    @Test
    public void testRandomNumberLessThanMax() {
        assertTrue("randomNumber is bigger than max", max >= randomNumber);
    }
}
