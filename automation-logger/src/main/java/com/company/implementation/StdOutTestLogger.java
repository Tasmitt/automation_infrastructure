package src.main.java.com.company.implementation;

import src.main.java.com.company.api.TestLogger;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by tasmitt on 15-Jul-17.
 */
public class StdOutTestLogger implements TestLogger{

        private int step = 1;

        @Override
        public void log(String message) {
            String output = step +") " + timeNow() + " [" + currentProcess() + "]: " + message;
            System.out.println(output);
            step++;
        }

        private String timeNow() {
            Date date = new Date();
            SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss.SSS");
            return sdf.format(date);
        }

        private String currentProcess() {
            return Thread.currentThread().getName();
        }
}
