package src.main.java.com.company.implementation;

import src.main.java.com.company.api.TestLogger;

import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by _tasmitt_ on 14/9/2016.
 */
public class FileTestLogger implements TestLogger{
    private int step = 1;
    private final String thread = Thread.currentThread().getName();
    private final long time = System.currentTimeMillis();

    @Override
    public void log(String message){
        try {
            FileWriter fileWriter = new FileWriter(thread+" "+time+".txt", true);
            fileWriter.write(step +") " + timeNow() + " [" + currentProcess() + "]: " + message + System.getProperty("line.separator"));
            fileWriter.close();
            step++;
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
    private String timeNow() {
        Date date = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss.SSS");
        return sdf.format(date);
    }

    private String currentProcess() {
        return Thread.currentThread().getName();
    }


}
