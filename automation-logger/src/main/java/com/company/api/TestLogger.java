package src.main.java.com.company.api;

public interface TestLogger {
    void log(String message);
}
