package com.company.automation.factory;

import com.company.automation.ConfigurationManager;
import com.company.automation.enums.BrowserType;
import com.company.automation.api.TestBrowserCreator;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

/**
 * Created by HillelWin8 on 22.09.2016.
 */
public class TestBrowserCreatorBuildserver implements TestBrowserCreator{



    @Override
    public WebDriver create() {
        BrowserType br = BrowserType.valueOf(ConfigurationManager.getTestBrowser().toUpperCase());
        WebDriver browser = null;
        switch (br){
            case CHROME:
                browser = new ChromeDriver();
                break;
            case FIREFOX:
                browser = new ChromeDriver();
                break;
            case SAFARI:
                browser = new ChromeDriver();
                break;
            case IEXPLORER:
                browser = new ChromeDriver();
                break;
            default:
                browser = new ChromeDriver();
                break;
        }
        return browser;
    }
}
