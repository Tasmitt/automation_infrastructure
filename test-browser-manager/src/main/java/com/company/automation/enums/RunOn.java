package com.company.automation.enums;

/**
 * Created by HillelWin8 on 22.09.2016.
 */
public enum RunOn {
    LOCAL,
    BUILDSERVER,
    REMOTE;
}
