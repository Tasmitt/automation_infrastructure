package com.company.automation.enums;

/**
 * Created by HillelWin8 on 22.09.2016.
 */
public enum BrowserType {
    CHROME,
    FIREFOX,
    IEXPLORER,
    SAFARI;
}
