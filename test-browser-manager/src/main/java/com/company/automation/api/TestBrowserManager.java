package com.company.automation.api;

import org.openqa.selenium.WebDriver;

/**
 * Created by HillelWin8 on 22.09.2016.
 */
public interface TestBrowserManager {
    WebDriver getTestBrowser();
    void destroyTestBrowser();
}
