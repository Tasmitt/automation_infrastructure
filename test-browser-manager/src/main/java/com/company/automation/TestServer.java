package com.company.automation;

import com.company.automation.ConfigurationManager;

/**
 * Created by _tasmitt_ on 8/10/2016.
 */
public class TestServer {
    public String getTestServer() {
        if (ConfigurationManager.getTestServer() == "production") {
            return "http://poductiondomain.com";

        } else {
            return "http://testdomain.com";
        }
    }
}
