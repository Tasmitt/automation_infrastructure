package com.company.automation;

import com.company.automation.enums.BrowserType;
import com.company.automation.api.TestBrowserCreator;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.safari.SafariDriver;

/**
 * Created by HillelWin8 on 22.09.2016.
 */
public class TestBrowserCreatorLocal implements TestBrowserCreator {

    @Override
    public WebDriver create() {
        BrowserType br = BrowserType.valueOf(ConfigurationManager.getTestBrowser().toUpperCase());
        WebDriver browser = null;
        switch (br){
            case CHROME:
                browser = new ChromeDriver();
            break;
            case FIREFOX:
                browser = new FirefoxDriver();
            break;
            case SAFARI:
                browser = new SafariDriver();
            break;
            case IEXPLORER:
                browser = new InternetExplorerDriver();
            break;
            default:
                browser = new ChromeDriver();
            break;
        }
        return browser;
    }
}
