package com.company.automation;

import com.company.automation.enums.RunOn;
import com.company.automation.factory.TestBrowserCreatorBuildserver;
import com.company.automation.factory.TestBrowserCreatorLocal;
import com.company.automation.factory.TestBrowserCreatorRemote;
import com.company.automation.api.TestBrowserCreator;
import com.company.automation.api.TestBrowserManager;
import com.company.automation.ConfigurationManager;
import org.openqa.selenium.WebDriver;

/**
 * Created by HillelWin8 on 22.09.2016.
 */
public class TestBrowserManagerImp implements TestBrowserManager{

    @Override
    public WebDriver getTestBrowser() {
        RunOn r = RunOn.valueOf(ConfigurationManager.getRunOn().toUpperCase());
        TestBrowserCreator testBrowser = null;
        switch (r){
            case BUILDSERVER:
               testBrowser = new TestBrowserCreatorBuildserver();
            break;
            case LOCAL:
                testBrowser = new TestBrowserCreatorLocal();
            break;
            case REMOTE:
                testBrowser = new TestBrowserCreatorRemote();
                break;
            default:
                System.out.println("No such environment");;
            break;
        }
        return testBrowser.create();

    }

    @Override
    public void destroyTestBrowser() {
        System.out.println("Test browser destroyed");
    }
}
