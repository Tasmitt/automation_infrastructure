package com.company.automation;

/**
 * Created by _tasmitt_ on 28/9/2016.
 */
public class ConfigurationManager {
    private static final String TEST_BROWSER = "testBrowser";
    private static final String TEST_SERVER = "testEnvironment";
    private static final String RUN_ON = "testRunOn";



    private ConfigurationManager() {    };

    private static ConfigurationManager instance = null;


    public ConfigurationManager getInstance() {
        if(instance == null){
            instance = new ConfigurationManager();
        }
        return instance;
    }

    public static String getTestBrowser(){
        return getEnvVar(TEST_BROWSER, "Chrome");
    };

    public static String getTestServer(){
        return getEnvVar(TEST_SERVER,"production");
    };

    public static String getRunOn(){
        return getEnvVar(RUN_ON,"Local");
    };

    private static String getEnvVar(String var, String defaultValue){
        if (System.getenv(var) != null){
            return var;
        } else {
            return defaultValue;
        }
    }
}
