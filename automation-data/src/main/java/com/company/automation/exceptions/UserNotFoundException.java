package com.company.automation.exceptions;

/**
 * Created by _tasmitt_ on 8/10/2016.
 */
public class UserNotFoundException extends RuntimeException {
    public UserNotFoundException() {
    }

    public UserNotFoundException(String message) {
        super(message);
    }
}
