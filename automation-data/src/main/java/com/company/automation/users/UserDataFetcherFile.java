package com.company.automation.users;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import com.company.automation.exceptions.UserNotFoundException;

/**
 * Created by _tasmitt_ on 8/10/2016.
 */
public class UserDataFetcherFile implements UserDataFetcher{
    private List<User> users = new ArrayList<>();
    public UserDataFetcherFile(){
        try{
            BufferedReader br = new BufferedReader( new FileReader("C:\\Users\\_tasmitt_\\IdeaProjects\\EnvV\\src\\User\\users.txt"));
            String line = null;

            while ((line = br.readLine()) != null){};
            String[] data = line.split(",");
            users.add(new User(data[0],data[1],data[2]));
        } catch (FileNotFoundException e) {
            throw new RuntimeException("File Users not found");
        } catch (IOException e) {
            throw new RuntimeException("File can't be read");
        }
    };
    @Override
    public User getUserByEmail(String email) {
        User user = null;

        for (User u: users){
            if (u.getEmail().equals(email));
            user = u;
        }

        if (user == null){
            throw new UserNotFoundException("User with email " + email + " was not found");
        }

        return user;
    }

    @Override
    public User getUserByName(String name) {
        User user = null;

        for (User u: users){
            if (u.getUserName().equals(name));
            user = u;
        }

        if (user == null){
            throw new UserNotFoundException("User with email " + name + " was not found");
        }

        return user;
    }

}
