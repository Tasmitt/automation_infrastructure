package com.company.automation.users;

/**
 * Created by _tasmitt_ on 8/10/2016.
 */
public interface UserDataFetcher {
    User getUserByEmail(String email);
    User getUserByName(String name);
}
