package com.company.ExecutionListener;

import org.junit.runner.Description;
import org.junit.runner.Result;
import org.junit.runner.notification.Failure;
import org.junit.runner.notification.RunListener;
import org.openqa.selenium.WebDriver;
import src.main.java.com.company.implementation.FileTestLogger;
import src.main.java.com.company.implementation.StdOutTestLogger;

/**
 * Created by tasmitt on 15-Jul-17.
 */
public class ExecutionListener extends RunListener {
    StdOutTestLogger logOut = new StdOutTestLogger();
    FileTestLogger logToFile = new FileTestLogger();
    Integer failCount=0;

    /**
     * Called before any tests have been run.
     * */
    public void testRunStarted(Description description)	throws java.lang.Exception
    {
        String log;
        log = ("Number of testcases to execute : " + description.testCount());
        logOut.log(log);
        logToFile.log(log);
    }

    /**
     *  Called when all tests have finished
     * */
    public void testRunFinished(Result result) throws java.lang.Exception
    {
        String log;
        log = ("Number of testcases executed : " + result.getRunCount()+" / failed : " + failCount);
        logOut.log(log);
        logToFile.log(log);
    }

    /**
     *  Called when an atomic test is about to be started.
     * */
    public void testStarted(Description description) throws java.lang.Exception
    {
        String log;
        log = ("Starting execution of test case : "+ description.getMethodName());
        logOut.log(log);
        logToFile.log(log);

    }

    /**
     *  Called when an atomic test has finished, whether the test succeeds or fails.
     * */
    public void testFinished(Description description) throws java.lang.Exception
    {
        String log;
        log = ("Finished execution of test case : "+ description.getMethodName());
        logOut.log(log);
        logToFile.log(log);
    }

    /**
     *  Called when an atomic test fails.
     * */
    public void testFailure(Failure failure) throws java.lang.Exception
    {
        String log;
 //       Screenshot screenshot = new Screenshot();
        log = ("Execution of test case failed : "+ failure.getMessage());
        logOut.log(log);
        logToFile.log(log);
        failCount++;
 //       screenshot.getScreenshot(driver);
    }

    /**
     *  Called when a test will not be run, generally because a test method is annotated with Ignore.
     * */
    public void testIgnored(Description description) throws java.lang.Exception
    {
        String log;
        log = ("Execution of test case ignored : "+ description.getMethodName());
        logOut.log(log);
        logToFile.log(log);
    }

}
