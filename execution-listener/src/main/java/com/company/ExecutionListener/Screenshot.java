package com.company.ExecutionListener;

import org.apache.commons.io.FileUtils;
import org.junit.Test;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by tasmitt on 16-Jul-17.
 */
public class Screenshot {
    public void getScreenshot(WebDriver webDriver){
    {

        try {
            File scrnsht = ((TakesScreenshot)webDriver).getScreenshotAs(OutputType.FILE);
            FileUtils.copyFile(scrnsht, new File("/tmp/"+timeNow()+".png"));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    }
    private String timeNow() {
        Date date = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss.SSS");
        return sdf.format(date);
    }
}
