package com.company;

import com.company.App.App;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
/**
 * Created by _tasmitt_ on 22/11/2016.
 */
public class AppTest {
    public static void main(String[] args) throws InterruptedException {
        WebDriver webDriver = new ChromeDriver();
        App app = App.getInstance();
        app.setWebDriver(webDriver);
        WebDriverWait wait;
        webDriver.get("https://j.superstream.com:8443/websti/#settings//");
        wait = new WebDriverWait(webDriver, 30);
        wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("button[type=button]")));

        webDriver.findElement(By.cssSelector("input[type=text]")).click();
        webDriver.findElement(By.cssSelector("input[type=text]")).sendKeys("wstitest_ext");

        webDriver.findElement(By.cssSelector("input[type=password]")).click();
        webDriver.findElement(By.cssSelector("input[type=password]")).sendKeys("wstitest_ext");

        webDriver.findElement(By.cssSelector("button[type=button]"));
        webDriver.findElement(By.cssSelector("button[type=button]")).click();

        wait.until(ExpectedConditions.presenceOfElementLocated(By.id("double_click_send")));
        System.out.println(webDriver.findElement(By.id("single_click_send")).findElement(By.cssSelector("input")).getAttribute("checked"));
    }
}
