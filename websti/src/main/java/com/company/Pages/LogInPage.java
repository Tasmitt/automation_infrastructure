package com.company.Pages;

import com.gargoylesoftware.htmlunit.ElementNotFoundException;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * Created by _tasmitt_ on 16/11/2016.
 */
public class LogInPage {
    private WebDriver webDriver;
    private WebDriverWait wait;


    public LogInPage(WebDriver driver) {
        webDriver = driver;
        wait = new WebDriverWait(webDriver, 60);
    }

    public WebElement usernameField(){
        try {
            return webDriver.findElement(By.cssSelector("input[type=text]"));

        } catch (Exception ex){
            throw new RuntimeException("Login field not found");
        }
    }
    public WebElement passField(){
        try {
            return webDriver.findElement(By.cssSelector("input[type=password]"));
        } catch (Exception ex){
            throw new RuntimeException("Password field not found");
        }
    }

    public WebElement loginButton(){
        try {
            return webDriver.findElement(By.cssSelector("button[type=button]"));
        } catch (Exception ex){
            throw new RuntimeException("Login button not found");
        }
    }

    public void logIn(String login, String pass){
        wait = new WebDriverWait(webDriver, 30);
        try{
            wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("button[type=button]")));
        }
        catch (Exception Ex){
            throw new RuntimeException("Login button not found or page did not load");
        }

        usernameField().click();
        usernameField().sendKeys(login);

        passField().click();
        passField().sendKeys(pass);

        loginButton().click();
    }

    public WebElement alertWindow(){
        By alertSelector = By.cssSelector(".col-xs-11.col-sm-4.alert.alert-info.animated.fadeInDown");
        try {
            wait.until(ExpectedConditions.presenceOfElementLocated(alertSelector));
        }catch (Exception ex){
            throw new RuntimeException("Alert window waiting timeout");
        }
        WebElement alertWindow = webDriver.findElement(alertSelector);
        return alertWindow;
    }

    public String alertWindowText(){
        By alertTextSelector = By.cssSelector("span[data-notify='message']");
        return alertWindow().findElement(alertTextSelector).getText();
    }

    public void addCurrency(String currency){
        currency.concat("/");
    }

    public String signInText(){return webDriver.findElement(By.cssSelector(".gwt-Label.signLabel")).getText();}

    public WebElement popupAlert(){return webDriver.findElement(By.cssSelector(".tooltip.fade.left.in"));}
}
