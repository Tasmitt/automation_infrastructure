package com.company.Pages;

import com.company.App.App;
import com.company.partials.ElementIsNot;
import com.company.partials.Radiobutton;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

/**
 * Created by _tasmitt_ on 16/11/2016.
 */
public class Settings {
    private WebDriver webDriver;
    private Wait wait;
    private App app;

    public Settings(WebDriver driver) {
        webDriver = driver;
        wait = new WebDriverWait(webDriver,30);
        app = App.getInstance();
        app.setWebDriver(webDriver);
    }

    public void settingsPageOpen(){
        webDriver.get("https://j.superstream.com:8443/websti2/#settings//");
        app.loginPage().logIn("wstitest_ext", "wstitest_ext");
        try {
            wait.until(ExpectedConditions.presenceOfElementLocated(By.className("settingsPanel")));
        }catch (Exception ex){
            throw new RuntimeException("Settings page header wait fail");
        }
    }

    public void settingsPageGet(){
        webDriver.get("https://j.superstream.com:8443/websti2/#settings//");
    }

    public void saveSettings(){
        try {
            wait.until(ExpectedConditions.presenceOfElementLocated(By.id("save_button")));
        }catch (Exception ex){
            throw new RuntimeException("Save settings button wait fail");
        }
       WebElement saveButton = webDriver.findElement(By.id("save_button"));
        ((JavascriptExecutor) webDriver).executeScript("arguments[0].scrollIntoView(true);", saveButton);
        saveButton.click();
    }

    public void resetSettings() {
        wait.until(ElementIsNot.elementNotPresent(webDriver, app.alerts().popupSettingsSavedSelector()));
        wait.until(ExpectedConditions.presenceOfElementLocated(By.className("gwt-Label")));

        WebElement commonSettingsElement= null;
        List<WebElement> labels = webDriver.findElements(By.className("gwt-Label"));
        for (WebElement lbl:labels){
            if (lbl.getText().equals("Common")){
                commonSettingsElement = lbl;
            }else {
                continue;
            }
        }
        if(commonSettingsElement==null){
            app.navBar().settingsButton().click();
        }

        By resetButtonSelector = By.id("reset_settings");
        wait.until(ExpectedConditions.presenceOfElementLocated(resetButtonSelector));
        ((JavascriptExecutor) webDriver).executeScript("arguments[0].scrollIntoView(true);", webDriver.findElement(resetButtonSelector));
        webDriver.findElement(resetButtonSelector).click();

        wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(".modal.fade.in")));
        WebElement confirmButton = webDriver.findElement(By.cssSelector(".modal.fade.in")).findElement(By.cssSelector(".btn.btn-primary"));
        confirmButton.click();

        wait.until(ElementIsNot.elementNotPresent(webDriver, By.cssSelector(".modal.fade")));
//        wait.until(ExpectedConditions.attributeToBe(webDriver.findElement(By.cssSelector(".modal.fade")), "display", "none"));

    }

    public void switchTab(String tabId) {
        try {
            wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(".col-sm-2.col-xs-1.box-shadow")));
        } catch (Exception ex){
            throw new RuntimeException("Settings panel wasn't detected");
        }
        WebElement tabElement = webDriver.findElement(By.id(tabId)).findElement(By.className("fa"));
        try {
            tabElement.click();
        } catch (Exception ex){
            ((JavascriptExecutor) webDriver).executeScript("arguments[0].scrollIntoView(true);", tabElement);

        }
    }


    public String getCurrentTab(){
        wait.until(ExpectedConditions.presenceOfElementLocated(By.className("settings-menu-item")));
        return webDriver.findElement(By.cssSelector(".gwt-Label.GFW5BATBGS")).getText();
    }

    public void switchToCommonTab(){
        wait.until(ExpectedConditions.presenceOfElementLocated(By.className("settings-menu-item")));
        webDriver.findElement(By.id("1")).findElement(By.className("fa")).click();
    }

}
