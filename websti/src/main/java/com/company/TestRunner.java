package com.company;

import com.company.ExecutionListener.ExecutionListener;
import org.junit.runner.JUnitCore;

/**
 * Created by tasmitt on 15-Jul-17.
 */
public class TestRunner {
    public static void main(String[] args) {
        JUnitCore runner = new JUnitCore();

        runner.addListener(new ExecutionListener());
        runner.run(SettingsSuite.class);
    }
}
