package com.company;

import com.company.App.App;
import com.company.automation.TestBrowserManagerImp;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import static org.junit.Assert.assertTrue;

/**
 * Created by _tasmitt_ on 17/11/2016.
 */
public class TestWebsti {
    private WebDriver webDriver;
    private WebDriverWait wait;
    private App app;

    public TestWebsti() {
        this.webDriver = new TestBrowserManagerImp().getTestBrowser();
        app = App.getInstance();
        app.setWebDriver(webDriver);
    }


    @Before
    public void setUp() {
        wait = new WebDriverWait(webDriver, 60);
        webDriver.get("https://j.superstream.com:8443/websti/");
        webDriver.get("https://j.superstream.com:8443/websti/#settings//");
        app.loginPage().logIn("wstitest_ext", "wstitest_ext");
    }

    @After
    public void cleanUp(){
        webDriver.quit();
    }

    @Test
    public void t() throws InterruptedException {
        wait.until(ExpectedConditions.presenceOfElementLocated(By.id("navbar-collapse")));

        Thread.sleep(30000);
        webDriver.findElement(By.cssSelector(".col-sm-12.col-xs-12"));


    }
}
