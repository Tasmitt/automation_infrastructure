package com.company;

import com.company.App.App;
import com.company.automation.TestBrowserManagerImp;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.Arrays;
import java.util.Collection;

/**
 * Created by Tasmitt on 2/1/2017.
 */
@RunWith(Parameterized.class)
public class SettingsInputFieldsPositiveTest {
    private WebDriver webDriver;
    private WebDriverWait wait;
    private App app;
    String id;
    String tab;
    String value;
    String valueDef;


    public SettingsInputFieldsPositiveTest(String id, String tab, String value,String valueDef) {
        this.webDriver = new TestBrowserManagerImp().getTestBrowser();
        app = App.getInstance();
        app.setWebDriver(webDriver);
        wait = new WebDriverWait(webDriver, 60);
        this.id = id;
        this.tab = tab;
        this.value = value;
        this.valueDef = valueDef;
    }

    @Parameterized.Parameters
    public static Collection inputPositive() {
        return Arrays.asList(new Object[][]{
                {"history_storage_time", "1","25","6"},
                {"history_storage_time", "1","0","6"},
                {"history_storage_time", "1","365","6"},
                {"speed_bump", "1","25","75"},
                {"speed_bump", "1","1","75"},
                {"speed_bump", "1","5000","75"},
                {"ladder_incr", "4","25","0.1"},
                {"ladder_incr", "4","0.1","0.1"},
                {"ladder_incr", "4","99.9","0.1"},
                {"ladder_digits", "4","25","3"},
                {"ladder_digits", "4","1","3"},
                {"ladder_digits", "4","99","3"},
                {"base_slippage", "5","25.25","0.5"},
                {"base_slippage", "5","0.0001","0.5"},
                {"base_slippage", "5","99.9","0.5"},
                {"baseSlippageDS", "5","25","30"},
                {"baseSlippageDS", "5","1","30"},
                {"baseSlippageDS", "5","100","30"},
                {"aggrSlippageBox", "5","25.25","0.5"},
                {"aggrSlippageBox", "5","0.0001","0.5"},
                {"aggrSlippageBox", "5","99.9","0.5"},
                {"aggrSlippageDS", "5","25","30"},
                {"aggrSlippageDS", "5","1","30"},
                {"aggrSlippageDS", "5","100","30"},
                {"pw_right_marg", "7","25","3"},
                {"pw_right_marg", "7","0","3"},
                {"pw_right_marg", "7","99999","3"},
                {"pw_bottom_marg", "7","25","5"},
                {"pw_bottom_marg", "7","0","5"},
                {"pw_bottom_marg", "7","99999","5"},
        });
    }

    @Before
    public void setUp() {
        app.settings().settingsPageOpen();
    }

    @Test
    public void settingsInputPositiveSvRstTest() {

        app.settings().switchTab(tab);
        app.inputField(id).setValue(value);

        Assert.assertTrue("Value of " + id + " field was not set to " + value, app.inputField(id).getValue().equals(value));

        app.settings().saveSettings();

        wait.until(ExpectedConditions.presenceOfElementLocated(app.alerts().popupSettingsSavedSelector()));

        Assert.assertTrue("Alert success text is not correct", app.alerts().popupSettingsSavedText().equals("Settings are updated"));
        Assert.assertTrue("Value of " + id + " field wasn't saved", app.inputField(id).getValue().equals(value));

        app.alerts().popupSetSavedClose();
        app.settings().resetSettings();

        wait.until(ExpectedConditions.presenceOfElementLocated(app.alerts().popupSettingsSavedSelector()));
        Assert.assertTrue("Alert success text is not correct", app.alerts().popupSettingsSavedText().equals("Settings are updated"));

        app.settings().switchTab(tab);

        Assert.assertTrue("Value of '" + id+"' field wasn't reset", app.inputField(id).getValue().equals(valueDef));

    }

    @After
    public void cleanUp() {
        webDriver.quit();
    }
}
