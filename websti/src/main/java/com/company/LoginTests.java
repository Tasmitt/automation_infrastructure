package com.company;

import com.company.App.App;
import com.company.automation.TestBrowserManagerImp;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;


/**
 * Created by _tasmitt_ on 8/1/2017.
 */
public class LoginTests {
    private WebDriver webDriver;
    private WebDriverWait wait;
    private App app;


    public LoginTests() {
        this.webDriver = new TestBrowserManagerImp().getTestBrowser();
        app = App.getInstance();
        app.setWebDriver(webDriver);
        wait = new WebDriverWait(webDriver, 60);
    }


    @Before
    public void setUp() {
        webDriver.get("https://j.superstream.com:8443/websti/");
    }

    @Test
    public void logInSuccessTest() throws InterruptedException {
        app.loginPage().logIn("wstitest_ext", "wstitest_ext");
        Thread.sleep(500);

        Assert.assertTrue("NavBar is not displayed", app.navBar().navBarElement().isDisplayed());
        Assert.assertTrue("Wrong text or account type displayed", app.navBar().accountTypeName().equals("UAT/DEMO"));
        Assert.assertTrue("No User Login displayed at the page", app.navBar().userDropdownMenu().getText().equals("wstitest_ext"));
        Assert.assertTrue("Not a price window page",app.sectorBlock().sectorOverallBlock().isDisplayed());
    }

    @Test
    public void loginNoPass(){
        app.loginPage().logIn("wstitest_ext","");

        Assert.assertTrue("No alert window displayed",app.loginPage().popupAlert().isDisplayed());
        Assert.assertTrue("Wrong text displayed in the alert window", webDriver.findElement(By.className("tooltip-inner")).getText().equals("Please enter your password"));
        Assert.assertTrue("'Sign In' not found on the page", app.loginPage().signInText().equals("Sign In"));
        Assert.assertTrue("Username field doesn't hold inserted value", app.loginPage().usernameField().getAttribute("value").equals("wstitest_ext"));
        Assert.assertTrue("Password field is not empty",app.loginPage().passField().getAttribute("value").equals(""));
        Assert.assertTrue("Navigation bar is displayed",app.elementIsNot().displayed(webDriver,  By.id("navbar-collapse")));
    }

    @Test
    public void loginIncorPass(){
        app.loginPage().logIn("wstitest_ext","fjhvut tr");

        Assert.assertTrue("Alert is not displayed", app.loginPage().alertWindow().isDisplayed());
        Assert.assertTrue("Alert message is not displayed or correct", app.loginPage().alertWindowText().equals("Wrong username or password"));
        Assert.assertTrue("'Sign In' not found in the page", app.loginPage().signInText().equals("Sign In"));
        Assert.assertTrue("Username field doesn't hold inserted value", app.loginPage().usernameField().getAttribute("value").equals("wstitest_ext"));
        Assert.assertTrue("Password field doesn't hold inserted value", app.loginPage().passField().getAttribute("value").equals("fjhvut tr"));
        Assert.assertTrue("Navigation bar is displayed",app.elementIsNot().displayed(webDriver,By.id("navbar-collapse")));

    }

    @Test
    public void loginIncorUsername(){
        app.loginPage().logIn("fsdsfvv","wstitest_ext");

        Assert.assertTrue("Alert is not displayed", app.loginPage().alertWindow().isDisplayed());
        Assert.assertTrue("Alert message is not displayed or correct", app.loginPage().alertWindowText().equals("Wrong username or password"));
        Assert.assertTrue("'Sign In' not found in the page", app.loginPage().signInText().equals("Sign In"));
        Assert.assertTrue("Username field doesn't hold inserted value", app.loginPage().usernameField().getAttribute("value").equals("fsdsfvv"));
        Assert.assertTrue("Password field doesn't hold inserted value", app.loginPage().passField().getAttribute("value").equals("wstitest_ext"));

    }

    @Test
    public void loginNoUsername(){
        app.loginPage().logIn("","wstitest_ext");

        Assert.assertTrue("No alert window displayed",app.loginPage().popupAlert().isDisplayed());
        Assert.assertTrue("Wrong text displayed in the alert window", webDriver.findElement(By.className("tooltip-inner")).getText().equals("Please enter your username"));
        Assert.assertTrue("'Sign In' not found on the page", app.loginPage().signInText().equals("Sign In"));
        Assert.assertTrue("Username field is not empty", app.loginPage().usernameField().getAttribute("value").equals(""));
        Assert.assertTrue("Password field doesn't hold inserted value", app.loginPage().passField().getAttribute("value").equals("wstitest_ext"));

    }

    @After
    public void cleanUp(){
        webDriver.quit();
    }
}
