package com.company;

import com.company.App.App;
import com.company.automation.TestBrowserManagerImp;
import com.company.partials.ElementIsNot;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.Arrays;
import java.util.Collection;

/**
 * Created by Tasmitt on 2/1/2017.
 */

@RunWith(Parameterized.class)
public class SettingsChkBoxTests {
    private WebDriver webDriver;
    private WebDriverWait wait;
    private App app;
    String chb;
    String tab;


    public SettingsChkBoxTests(String checkbox, String tab) {
        this.webDriver = new TestBrowserManagerImp().getTestBrowser();
        app = App.getInstance();
        app.setWebDriver(webDriver);
        wait = new WebDriverWait(webDriver, 60);
        this.chb = checkbox;
        this.tab = tab;
    }

    @Parameterized.Parameters
    public static Collection chkBoxes() {
        return Arrays.asList(new Object[][] {
                { "diff_sizes_checkbox", "1" },
                { "show_orders_trans", "1"  },
                { "depth_book_exec","1"  },
                { "depth_book_sweep", "1" },
                { "conf_send_order", "2" },
                { "conf_cancel_order", "2" },
                { "conf_order_market", "2" },
                { "conf_cancel_all", "2" },
                { "conf_all_market", "2" },
                { "conf_exec_depth", "2" },
                { "conf_sweep_depth", "2" },
                { "conf_LMT_outside", "2" },
                { "show_full_price", "4" },
                { "customSlippageCheckBox", "5" },
                { "customTTLCheckBox", "5" },
                { "perTierTTLCheckBox", "5" },
                { "forceTTLforMKTCheckBox", "5" },
                { "filter_block", "6" },
                { "pw_placement", "7" },
                { "horizontal_ui", "7" }
    });
    }
    @Before
    public void setUp(){
        app.settings().settingsPageOpen();
    }

    @Test
    public void settingsCheckboxTest() throws InterruptedException {
        app.settings().switchTab(tab);
        app.checkBox(chb).clickChBox();

        app.settings().saveSettings();

        wait.until(ExpectedConditions.presenceOfElementLocated(app.alerts().popupSettingsSavedSelector()));
        Assert.assertTrue("Alert success text is not correct", app.alerts().popupSettingsSavedText().equals("Settings are updated"));

        wait.until(ExpectedConditions.presenceOfElementLocated(By.id(chb)));
        Assert.assertTrue("id='" + chb + "'" + "checkbox change wasn't saved", app.checkBox(chb).getStatus());

        app.alerts().popupSetSavedClose();
        app.settings().resetSettings();

        wait.until(ExpectedConditions.presenceOfElementLocated(app.alerts().popupSettingsSavedSelector()));
        Assert.assertTrue("Alert success text is not correct", app.alerts().popupSettingsSavedText().equals("Settings are updated"));

        app.settings().switchTab(tab);
        wait.until(ExpectedConditions.presenceOfElementLocated(By.id(chb)));

        Assert.assertFalse("id='" + chb + "'" + "checkbox change wasn't reset", app.checkBox(chb).getStatus());
    }

    @After
    public void cleanUp(){
        webDriver.quit();
    }
}
