package com.company;

import com.company.App.App;
import com.company.automation.TestBrowserManagerImp;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.Arrays;
import java.util.Collection;

/**
 * Created by Tasmitt on 2/1/2017.
 */
@RunWith(Parameterized.class)
public class SettingsInputFieldsNegativeTests {
    private WebDriver webDriver;
    private WebDriverWait wait;
    private App app;
    String id;
    String tab;
    String value;
    String valueDef;
    String message;


    public SettingsInputFieldsNegativeTests(String id, String tab, String value,String valueDef, String message) {
        this.webDriver = new TestBrowserManagerImp().getTestBrowser();
        app = App.getInstance();
        app.setWebDriver(webDriver);
        wait = new WebDriverWait(webDriver, 60);
        this.id = id;
        this.tab = tab;
        this.value = value;
        this.valueDef = valueDef;
        this.message = message;
    }

    @Parameterized.Parameters
    public static Collection inputNegative() {
        return Arrays.asList(new Object[][]{
                {"history_storage_time", "1","366","6", "Value must be in range from 0 to 365"},
                {"speed_bump", "1","5001","75","Value must be in range from 1 to 5000"},
                {"speed_bump", "1","0","75","Value must be in range from 1 to 5000"},
                {"ladder_incr", "4","100","0.1","Value must be in range from 0.1 to 99.9"},
                {"ladder_incr", "4","0","0.1","Value must be in range from 0.1 to 99.9"},
                {"ladder_digits", "4","100","3","Value must be in range from 1 to 99"},
                {"ladder_digits", "4","0","3","Value must be in range from 1 to 99"},
                {"base_slippage", "5","0","0.5", "Value must be in range from 0.0001 to 99.9"},
                {"base_slippage", "5","100","0.5", "Value must be in range from 0.0001 to 99.9"},
                {"base_slippage", "5","99.99","0.5", "Value must be in range from 0.0001 to 99.9"},
                {"baseSlippageDS", "5","101","30","Value must be in range from 1 to 100"},
                {"baseSlippageDS", "5","0","30","Value must be in range from 1 to 100"},
                {"aggrSlippageBox", "5","0","0.5", "Value must be in range from 0.0001 to 99.9"},
                {"aggrSlippageBox", "5","100","0.5", "Value must be in range from 0.0001 to 99.9"},
                {"aggrSlippageBox", "5","99.99","0.5", "Value must be in range from 0.0001 to 99.9"},
                {"aggrSlippageDS", "5","101","30","Value must be in range from 1 to 100"},
                {"aggrSlippageDS", "5","0","30","Value must be in range from 1 to 100"},
                {"pw_right_marg", "7","100000","3","Value must be in range from 0 to 99999"},
                {"pw_bottom_marg", "7","100000","5","Value must be in range from 0 to 99999"},

        });
    }

    @Before
    public void setUp() {
        app.settings().settingsPageOpen();
    }

    @Test
    public void settingsInputFieldsNegativeTests() {
        app.settings().switchTab(tab);
        app.inputField(id).setValue(value);
        app.settings().saveSettings();

        Assert.assertTrue("Error message for'"+id+"' field  text is not correct or not displayed",app.alerts().inputFieldErrorMessage().equals(message));

        app.settings().resetSettings();
        app.settings().switchTab(tab);

        Assert.assertTrue("Value for '"+id+"' field wasn't reset to default",app.inputField(id).getValue().equals(valueDef));


    }

    @After
    public void cleanUp() {
        webDriver.quit();
    }
}
