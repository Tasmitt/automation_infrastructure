package com.company.blocks;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

/**
 * Created by _tasmitt_ on 16/11/2016.
 */
public class NavBar {
    private  WebDriver webDriver;
    private WebDriverWait wait;


    public NavBar(WebDriver driver) {
        webDriver = driver;
        wait = new WebDriverWait(webDriver, 60);
    }

    public WebElement userDropdownMenu(){
        try {



        wait.until(ExpectedConditions.presenceOfElementLocated(By.className("dropdown-toggle")));
        } catch (Exception ex){
            throw new RuntimeException("User menu wait fail");
        }

        WebElement block = null;
        List< WebElement > carets = webDriver.findElements(By.className("dropdown-toggle"));
        for(WebElement e:carets){
            try {
                e.findElement(By.cssSelector(".fa.fa-user"));
                block = e;
            } catch (Exception ex){
                continue;
            }

        }
        return block;
    }

    public WebElement navBarElement(){
        try {
            wait.until(ExpectedConditions.presenceOfElementLocated(By.id("navbar-collapse")));
        } catch (Exception ex){
            throw new RuntimeException("Navigation bar wait fail");
        }
        return webDriver.findElement( By.id("navbar-collapse"));
    }


    public String accountTypeName(){
       return webDriver.findElement(By.className("col-xs-12")).findElement(By.cssSelector("nav > div > div")).getText();
    }

    public WebElement settingsButton(){
        userDropdownMenu().click();
        WebElement settingsButton = webDriver.findElement(By.cssSelector(".dropdown.open")).findElement(By.cssSelector(".dropdown-menu>li>a"));
        if(settingsButton.getText().equals("Settings")){
            return settingsButton;
        } else{
            throw new RuntimeException("Settings button wasn't found");
        }
    }
}
