package com.company.blocks;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * Created by _tasmitt_ on 16/11/2016.
 */
public class SectorBlock {
    private WebDriver webDriver;
    private WebDriverWait wait;


    public SectorBlock(WebDriver driver) {
        webDriver = driver;
        wait = new WebDriverWait(webDriver, 60);
    }

    public WebElement sectorOverallBlock(){

        try{ wait.until(ExpectedConditions.presenceOfElementLocated(By.id("sortable")));
        } catch (Exception ex){
           throw  new RuntimeException("Sector containing block was not found");}

        return webDriver.findElement(By.id("sortable"));

    }


}
