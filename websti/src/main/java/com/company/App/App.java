package com.company.App;

import com.company.Pages.*;
import com.company.automation.ConfigurationManager;
import com.company.automation.TestBrowserManagerImp;
import com.company.blocks.NavBar;
import com.company.blocks.OrderTable;
import com.company.blocks.SectorBlock;
import com.company.partials.*;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import java.security.Timestamp;

/**
 * Created by HillelWin8 on 20.11.2016.
 */
public class App {
    private WebDriver webDriver;

    private App() {   };

    private static App instance = null;

    public static App getInstance(){
        if(instance == null){
            instance = new App();
        }
        return instance;
    }

    public  void setWebDriver(WebDriver webDriver){
        this.webDriver = webDriver;
    }

    public LogInPage loginPage(){ return new LogInPage(webDriver);}
    public OrderTableWindow orderTable(){  return new OrderTableWindow(webDriver);}
    public PriceWindow priceWindow(){  return new PriceWindow(webDriver);}
    public Quoteboard quoteboard(){  return new Quoteboard(webDriver);}
    public BugReportForm bugReportForm(){  return new BugReportForm(webDriver);}
    public Settings settings(){  return new Settings(webDriver);}

    public NavBar navBar(){ return new NavBar(webDriver);}
    public SectorBlock sectorBlock(){ return new SectorBlock(webDriver);}

    public Radiobutton radiobutton(String id){  return new Radiobutton(webDriver, id);}
    public CheckBox checkBox(String id){  return new CheckBox(webDriver, id);}
    public InputField inputField(String id){return new InputField(webDriver, id);}
    public Alerts alerts(){return new Alerts(webDriver);}
    public ElementIsNot elementIsNot(){return new ElementIsNot(webDriver);}
    public Timestamps timestamps(){return new Timestamps(webDriver);}
}
