package com.company.partials;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * Created by _tasmitt_ on 23/11/2016.
 */
public class InputField {
    private WebDriver webDriver;
    private WebDriverWait wait;
    private String id;

    public InputField(WebDriver driver, String id) {
        webDriver = driver;
        wait = new WebDriverWait(webDriver, 30);
        this.id = id;
    }

    private WebElement input(){
        wait.until(ExpectedConditions.presenceOfElementLocated(By.id(id)));
        return webDriver.findElement(By.id(id));
    }


    public String getValue(){
       return input().getAttribute("value");
    }

    public void setValue(String value){
        input().click();
        input().clear();
        if (input().getText().equals("")){
        input().sendKeys(value);}
        else {
            input().clear();
            input().sendKeys(value);
        }
    }


}
