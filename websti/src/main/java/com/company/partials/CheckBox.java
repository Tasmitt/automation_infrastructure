package com.company.partials;

import com.company.App.App;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

/**
 * Created by _tasmitt_ on 23/11/2016.
 */
public class CheckBox {
    private WebDriver webDriver;
    private WebDriverWait wait;
    private String text;
    private App app;


    public CheckBox(WebDriver driver, String text) {
        webDriver = driver;
        app = App.getInstance();
        app.setWebDriver(webDriver);
        wait = new WebDriverWait(webDriver, 30);
        this.text = text;
    }

    private WebElement checkbox(String id){
        wait.until(ExpectedConditions.presenceOfElementLocated(By.id(id)));

        WebElement checkBox = webDriver.findElement(By.id(id));
        try {
            return checkBox.findElement(By.tagName("input"));
        } catch (Exception ex) {
            throw new RuntimeException("'"+checkboxText(text)+"' checkbox wasn't found");
        }

    }

    public void clickChBox() throws InterruptedException {
        try {
            checkbox(text).click();
        } catch (WebDriverException ex) {
            webDriver.findElement(By.cssSelector(".form-control.pull-left")).click();
            wait.until(ElementIsNot.elementNotPresent(webDriver,  By.cssSelector("[id*=tooltip]")));
 //           app.settings().clickSettingsHeader();
 //           Thread.sleep(100);
            checkbox(text).click();
        } catch (Exception ex) {
            throw new RuntimeException("'"+checkboxText(text)+"' checkbox wasn't clicked");
        }
    }

    public boolean getStatus() throws RuntimeException {
        if (checkbox(text).getAttribute("checked") != null) {
            return true;
        } else {
            return false;
        }
    }
    private String checkboxText(String id){
        wait.until(ExpectedConditions.presenceOfElementLocated(By.id(id)));

        WebElement checkBox = webDriver.findElement(By.id(id));
        return checkBox.findElement(By.tagName("span")).getText();

    }

//    public void click() {
//        try {
//            wait.until(ExpectedConditions.presenceOfElementLocated(By.id(id)));
//            webDriver.findElement(By.id(id)).click();
//        } catch (Exception ex) {
//            new RuntimeException("Checkbox wasn't clicked");
//        }
//    }

//    public boolean getStatus() throws RuntimeException {
//        wait.until(ExpectedConditions.presenceOfElementLocated(By.id(id)));
//        if (webDriver.findElement(By.id(id)).getAttribute("checked") != null) {
//            return true;
//        } else {
//            return false;
//        }
//    }
}