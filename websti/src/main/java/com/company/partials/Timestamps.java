package com.company.partials;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.Locale;

/**
 * Created by Tasmitt on 1/19/2017.
 */
public class Timestamps {
    private WebDriver webDriver;
    public Timestamps(WebDriver driver){
        webDriver = driver;
    }

    public String timestampHHmmss(){
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("HH:mm", Locale.US);
        LocalTime time = LocalTime.now();
        String f = formatter.format(time);
        return f;
    }

    public String notificationsTimestamp(){
        String t = webDriver.findElement(By.className("notificationMessageDate")).getText();
        return t.substring(0, t.length()-3);
    }



}
