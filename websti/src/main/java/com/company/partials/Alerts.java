package com.company.partials;


import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.ArrayList;

/**
 * Created by Tasmitt on 1/18/2017.
 */
public class Alerts {
    private WebDriver webDriver;
    private WebDriverWait wait;
    private By popupSelector = By.cssSelector(".tooltip.fade.right.in");
    private By notificationButtonSelector = By.cssSelector(".btn.btn-default.navbar-btn.pull-left.notificationMessageUnreader");
    private By notificationSignSelector = By.className("gwt-HTML");

    public Alerts(WebDriver driver) {
        webDriver = driver;
        wait = new WebDriverWait(webDriver,30);
    }

    public WebElement popupAlert(){
        return webDriver.findElement(popupSelector);
    }

    public String inputFieldErrorMessage(){
        wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(".form-group.has-error")));
        return webDriver.findElement(By.cssSelector(".form-group.has-error")).findElement(By.className("help-block")).getText();
    }

    public boolean popupAlertNotPresent(){
        return webDriver.findElements(popupSelector).size()==0;
    }

    public String popupAlertText(){
        return popupAlert().findElement(By.className("tooltip-inner")).getText();
    }

    public WebElement notificationButton(){
       return webDriver.findElement(notificationButtonSelector);
    }

    public boolean notificationButtonNotPresent(){
        return webDriver.findElements(notificationButtonSelector).size()==0;
    }

    public WebElement notificationSign(){
        return webDriver.findElement(notificationSignSelector);
    }

    public boolean notificationSignNotPresent(){
        return webDriver.findElements(notificationSignSelector).size()==0;
    }

    public WebElement notificationBadge(){
        return webDriver.findElement(By.className("badge"));
    }

    public By popupSettingsSavedSelector(){return By.cssSelector(".col-xs-11.col-sm-4.alert.alert-success.animated.fadeInDown");}

    private WebElement popupSettingsSaved(){return webDriver.findElement(popupSettingsSavedSelector());}

    public void popupSetSavedClose(){
        popupSettingsSaved().findElement(By.tagName("button")).click();
    }

    public String popupSettingsSavedText(){ return popupSettingsSaved().findElement(By.cssSelector("span[data-notify='message']")).getText();
    }

}
