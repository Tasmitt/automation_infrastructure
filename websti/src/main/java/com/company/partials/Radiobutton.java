package com.company.partials;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * Created by _tasmitt_ on 19/11/2016.
 */
public class Radiobutton {
    private WebDriver webDriver;
    private WebDriverWait wait;
    private String id;

    public Radiobutton(WebDriver driver, String id) {
        webDriver = driver;
        wait = new WebDriverWait(webDriver,30);
        this.id = id;
        }

    private WebElement radiobutton(){
        wait.until(ExpectedConditions.presenceOfElementLocated(By.id(id)));
        return webDriver.findElement(By.id(id)).findElement(By.cssSelector("input[type='radio']"));
    }



    public void click() {
        try {
            wait.until(ExpectedConditions.presenceOfElementLocated(By.id(id)));
            radiobutton().click();
        } catch (Exception ex) {
            new RuntimeException("Radibutton wasn't clicked");
        }
    }

    public boolean getStatus() {
            wait.until(ExpectedConditions.presenceOfElementLocated(By.id(id)));
            if (radiobutton().getAttribute("checked") != null) {
                return true;
            } else{
                return false;
            }
    }
}
