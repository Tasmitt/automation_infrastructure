package com.company.partials;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * Created by Tasmitt on 1/19/2017.
 */
public class ElementIsNot {
    private WebDriver webDriver;
    private WebDriverWait wait;


    public ElementIsNot(WebDriver driver) {
        webDriver = driver;
        wait = new WebDriverWait(webDriver, 30);
    }

    public static boolean displayed(WebDriver webDriver,By selector) {
        return webDriver.findElements(selector).size() == 0;
    }

    public static boolean visible(WebDriver webDriver,By selector) {
        return webDriver.findElement(selector).isDisplayed();
    }

    public static ExpectedCondition<Boolean> elementNotPresent(WebDriver webDriver, final By locator) {
        return new ExpectedCondition<Boolean>() {
            @Override
            public Boolean apply(WebDriver webDriver) {

                return webDriver.findElements(locator).size() == 0;
            }
        };
    }
}