package com.company;

import com.company.App.App;
import com.company.automation.TestBrowserManagerImp;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.sql.Time;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

/**
 * Created by _tasmitt_ on 13/1/2017.
 */
public class SettingsTests {
    private WebDriver webDriver;
    private WebDriverWait wait;
    private App app;


    public SettingsTests() {
        this.webDriver = new TestBrowserManagerImp().getTestBrowser();
        app = App.getInstance();
        app.setWebDriver(webDriver);
        wait = new WebDriverWait(webDriver, 60);
    }

    @Before
    public void setUp() {
        app.settings().settingsPageOpen();
    }


    @Test
    public void sendOrderBySvdRstTest() {
        String id = "single_click_send";
        String id2 = "double_click_send";
        String message = "Radio button status wasn't ";

        app.radiobutton(id).click();

        Assert.assertTrue(message + "changed", app.radiobutton(id).getStatus());
        Assert.assertFalse(message + "changed", app.radiobutton(id2).getStatus());

        app.settings().saveSettings();

        Assert.assertTrue(message + "saved", app.radiobutton(id).getStatus());
        Assert.assertFalse(message + "saved", app.radiobutton(id2).getStatus());

//        app.settings().resetSettings();
//
//        Assert.assertFalse(message + "reset", app.radiobutton(id).getStatus());
//        Assert.assertTrue(message + "reset", app.radiobutton(id2).getStatus());
    }

    @Test
    public void depthBookExecuteBySvdRstTest() {
        String id = "single_click_depth";
        String id2 = "double_click_depth";

        app.radiobutton(id).click();

        Assert.assertTrue("Radio button status wasn't changed", app.radiobutton(id).getStatus());
        Assert.assertFalse("Radio button status wasn't changed", app.radiobutton(id2).getStatus());

        app.settings().saveSettings();
        app.settings().settingsPageGet();

        Assert.assertTrue("Radio button status wasn't saved", app.radiobutton(id).getStatus());
        Assert.assertFalse("Radio button status wasn't saved", app.radiobutton(id2).getStatus());

        app.settings().resetSettings();

        Assert.assertFalse("Radio button status wasn't reset", app.radiobutton(id).getStatus());
        Assert.assertTrue("Radio button status wasn't reset", app.radiobutton(id2).getStatus());
    }


    @Test
    public void orderHistoryNoValueTest() {
        String id = "history_storage_time";

        app.inputField(id).setValue("");
        app.settings().saveSettings();

        Assert.assertTrue("Notification alert for history storage time is not highlighted or displayed", app.alerts().notificationButton().isDisplayed());
        Assert.assertTrue("Notification badge is not displayed", app.alerts().notificationBadge().isDisplayed());
        Assert.assertTrue("Notification badge number is not equal '1'", app.alerts().notificationBadge().getText().equals("1"));

        app.alerts().notificationButton().click();

        //Text must be changed to correct one
        Assert.assertTrue("Notification alert text for history storage time is either not correct or present", app.alerts().notificationSign().getText().equals("Field storage history can't be empty and must contain only digits"));
        Assert.assertTrue("Notification alert for history storage time is still highlighted or not displayed", app.alerts().notificationButtonNotPresent());

        Assert.assertTrue("Notification alert timestamp for history storage time is either not correct or present", app.timestamps().notificationsTimestamp().equals(app.timestamps().timestampHHmmss()));

        app.navBar().userDropdownMenu().click();

        Assert.assertTrue("Notification alert text for history storage time is still displayed", app.alerts().notificationSignNotPresent());
    }

    @Test
    public void clickSpeedBumpNoValueTest() {
        String id = "speed_bump";

        app.inputField(id).setValue("");
        app.settings().saveSettings();

        Assert.assertTrue("Notification alert for Click speed bump is not highlighted or displayed", app.alerts().notificationButton().isDisplayed());
        Assert.assertTrue("Notification badge is not displayed", app.alerts().notificationBadge().isDisplayed());
        Assert.assertTrue("Notification badge number is not equal '1'", app.alerts().notificationBadge().getText().equals("1"));

        app.alerts().notificationButton().click();

        //Text must be changed to correct one
        Assert.assertTrue("Notification alert text for Click speed bump is either not correct or present", app.alerts().notificationSign().getText().equals("Field \"Click speed bump\" cannot be empty and must contain only digits."));
        Assert.assertTrue("Notification alert for Click speed bump is still highlighted or not displayed", app.alerts().notificationButtonNotPresent());

        Assert.assertTrue("Notification alert timestamp for Click speed bump is either not correct or present", app.timestamps().notificationsTimestamp().equals(app.timestamps().timestampHHmmss()));

        app.navBar().userDropdownMenu().click();

        Assert.assertTrue("Notification alert text for Click speed bump is still displayed", app.alerts().notificationSignNotPresent());
    }

    @Test
    public void preferTierSlippageChkBoxSaveResetTest() throws InterruptedException {
        String chb = "preferTierSlippageCheckBox";
        String tab = "5";

        app.settings().switchTab(tab);

        app.checkBox(chb).clickChBox();

        app.settings().saveSettings();
        app.settings().settingsPageGet();   //needs to be rewritten
        app.settings().switchTab(tab);

        Assert.assertFalse("id='" + chb + "'" + "checkbox change wasn't saved", app.checkBox(chb).getStatus());

        app.settings().resetSettings();
        app.settings().switchTab(tab);

        Assert.assertTrue("id='" + chb + "'" + "checkbox change wasn't reset", app.checkBox(chb).getStatus());

    }

    @Test
    public void overwriteCustomSlippageChkBoxSaveResetTest() throws InterruptedException {
        String chb = "overwriteCustomSlippageAndTTLCheckBox";
        String tab = "5";

        app.settings().switchTab(tab);
        app.checkBox(chb).clickChBox();

        Assert.assertTrue("id='" + chb + "'" + "checkbox change wasn't changed", app.checkBox(chb).getStatus());

        app.settings().saveSettings();
        app.settings().settingsPageGet();   //needs to be rewritten
        app.settings().switchTab(tab);

        Assert.assertFalse("id='" + chb + "'" + "checkbox change wasn't saved", app.checkBox(chb).getStatus());
    }

    @Test
    public void openPriceLadderRadioSvdRstTest() {
        String id1 = "price_left_cl";
        String id2 = "price_right_cl";

        app.settings().switchTab("4");

        app.radiobutton(id1).click();

        Assert.assertTrue("Radio button status wasn't changed", app.radiobutton(id1).getStatus());
        Assert.assertFalse("Radio button status wasn't changed", app.radiobutton(id2).getStatus());

        app.settings().saveSettings();
        app.settings().settingsPageGet();

        Assert.assertTrue("Radio button status wasn't saved", app.radiobutton(id1).getStatus());
        Assert.assertFalse("Radio button status wasn't saved", app.radiobutton(id2).getStatus());

        app.settings().resetSettings();

        Assert.assertFalse("Radio button status wasn't reset", app.radiobutton(id1).getStatus());
        Assert.assertTrue("Radio button status wasn't reset", app.radiobutton(id2).getStatus());
    }


    @Test
    public void priceLadderIncrementsNoValueTest() {
        String id = "ladder_incr";

        app.settings().switchTab("4");
        app.inputField(id).setValue("");
        app.settings().saveSettings();

        Assert.assertTrue("Notification alert for 'Price Ladder increments' field is not highlighted or displayed", app.alerts().notificationButton().isDisplayed());
        Assert.assertTrue("Notification badge is not displayed", app.alerts().notificationBadge().isDisplayed());
        Assert.assertTrue("Notification badge number is not equal '1'", app.alerts().notificationBadge().getText().equals("1"));

        app.alerts().notificationButton().click();

        Assert.assertTrue("Notification alert text for 'Price Ladder increments' field is either not correct or present", app.alerts().notificationSign().getText().equals("Wrong price ladder increment value"));
        Assert.assertTrue("Notification alert for 'Price Ladder increments' field is still highlighted or not displayed", app.alerts().notificationButtonNotPresent());
        Assert.assertTrue("Notification alert timestamp for 'Price Ladder increments' field is either not correct or present", app.timestamps().notificationsTimestamp().equals(app.timestamps().timestampHHmmss()));

        app.navBar().userDropdownMenu().click();

        Assert.assertTrue("Notification alert text for 'Price Ladder increments' field is still displayed", app.alerts().notificationSignNotPresent());
    }

    @Test
    public void priceLadderDigitsNoValueTest() {
        String id = "ladder_digits";

        app.settings().switchTab("4");
        app.inputField(id).setValue("");
        app.settings().saveSettings();

        Assert.assertTrue("Notification alert for 'Price Ladder digits' field is not highlighted or displayed", app.alerts().notificationButton().isDisplayed());
        Assert.assertTrue("Notification badge is not displayed", app.alerts().notificationBadge().isDisplayed());
        Assert.assertTrue("Notification badge number is not equal '1'", app.alerts().notificationBadge().getText().equals("1"));

        app.alerts().notificationButton().click();

        Assert.assertTrue("Notification alert text for 'Price Ladder digits' field is either not correct or present", app.alerts().notificationSign().getText().equals("Wrong price ladder digits count value"));
        Assert.assertTrue("Notification alert for 'Price Ladder digits' field is still highlighted or not displayed", app.alerts().notificationButtonNotPresent());
        Assert.assertTrue("Notification alert timestamp for 'Price Ladder digits' field is either not correct or present", app.timestamps().notificationsTimestamp().equals(app.timestamps().timestampHHmmss()));

        app.navBar().userDropdownMenu().click();

        Assert.assertTrue("Notification alert text for 'Price Ladder digits' field is still displayed", app.alerts().notificationSignNotPresent());
    }

    @After
    public void cleanUp(){
        webDriver.quit();
    }
}
